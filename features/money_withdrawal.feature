Feature: Money Withdrawal

  Scenario: Enough money available
    Given the account balance is 100
    And the card is valid
    And the machine contains enough money
    When the account requests 20
    Then the cashpoint should dispense 20
    And the account balance should be 80
    And the card should be returned