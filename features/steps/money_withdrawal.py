from behave import *

import os


FILEPATH = os.path.realpath(__file__)
PRJ_PATH = os.path.dirname(os.path.dirname(os.path.dirname(FILEPATH)))
WORKSPACE_PATH = os.path.dirname(PRJ_PATH)


@given("the account balance is 100")
def step_impl(context):
    pass


@step("the card is valid")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@step("the machine contains enough money")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@when("the account requests 20")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@then("the cashpoint should dispense 20")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@step("the account balance should be 80")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@step("the card should be returned")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass

