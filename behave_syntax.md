# Behave
## file structure
features folder, contains steps folder.
features folder contains feature files
steps folder contains python files with step implementations

## Step implementation

### Context
context parameter is across scenarios steps of a scenario

### Pass a step
Just return pass to make a step succeed

### Fail a step
raise Exception("Exception message")

### Evaluate a step
assert keyword

### Step parameters
In gherkin between quotes

In step implementation => "{argument}" as text
and step_impl(context, argument)

## Step tables

Available in context argument:

for row in context.table:
	column = row['column_title']

## Execute feature files
* behave 
* behave [feature_file]
* -D foo=bar => stored in config.userdata['foo']
* -f json.pretty
* -o outputfile
* --tags=dev || --tags="not @dev"
* behave --lang-list
* behave --lang-help nl
* #language: nl at top of feature file

## Attributes
in environment.py in the features directory
* before_all()
* after_all()
* after_feature(context, feature) => feature.status
* after_scenario(context, scenario) => scenario.status
* ...


## Configuration file
There is a possibility to work with a configuration file. This file needs to have one of following names:
* .behaverc.ini
* behave.ini
* setup.cfg
* tox.ini

and this file must be located at one of following:
* current working directory
* home directory ($HOME)
* (on windows: %APPDATA% directory)

For implementation of the configuration I suggest to look at the behave tutorial which can be found online

