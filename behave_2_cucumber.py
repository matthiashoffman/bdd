import json
import behave2cucumber

def main():
    try:
        with open('reports/behave_report.json') as behave_json:
            cucumber_json = behave2cucumber.convert(json.load(behave_json), duration_format=True)
            with open('reports/output.json', 'w') as output_file:
                json.dump(cucumber_json, output_file)
    except Exception as exc:
        print exc

if __name__ == "__main__":
    main()